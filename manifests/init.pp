class iipsrv (
  $iipsrv_fcgi_directory = $iipsrv::params::iipsrv_fcgi_directory,
  $fileserver_mount      = $iipsrv::params::fileserver_mount,
  $iipsrv_user           = $iipsrv::params::iipsrv_user,
  $iipsrv_group          = $iipsrv::params::iipsrv_group,
) inherits iipsrv::params {

  # Note: memcached package should be installed separately, e.g. by memcached puppet module
  $required_packages = ['zlib-devel', 'libtiff-devel', 'libjpeg-turbo-devel', 'libmemcached']

  package {$required_packages:
    ensure => 'installed',
  }

  file { $iipsrv_fcgi_directory:
    ensure  => directory,
    owner  => $iipsrv_user,
    group  => $iipsrv_group,
  }

  file { "$iipsrv_fcgi_directory/logs":
    ensure  => directory,
    owner  => $iipsrv_user,
    group  => $iipsrv_group,
    require => File[$iipsrv_fcgi_directory],
  }

  file { "$iipsrv_fcgi_directory/apps":
    ensure  => directory,
    owner  => $iipsrv_user,
    group  => $iipsrv_group,
    require => File[$iipsrv_fcgi_directory],
  }

  file { "$iipsrv_fcgi_directory/apps/make":
    ensure  => directory,
    owner  => $iipsrv_user,
    group  => $iipsrv_group,
    require => File["$iipsrv_fcgi_directory/apps"],
  }

  file { 'kakadu_library':
    ensure  => present,
    owner  => $iipsrv_user,
    group  => $iipsrv_group,
    mode    => '0664',
    path    => "$iipsrv_fcgi_directory/apps/make/libkdu_v64R.so",
    source  => "puppet:///$fileserver_mount/libkdu_v64R.so",
    require => File["$iipsrv_fcgi_directory/apps/make"],
  }

  file { 'iipsrv_binary':
    ensure  => present,
    owner  => $iipsrv_user,
    group  => $iipsrv_group,
    mode    => '0755',
    path    => "$iipsrv_fcgi_directory/iipsrv.fcgi",
    source  => "puppet:///$fileserver_mount/iipsrv.fcgi",
    require => [File[$iipsrv_fcgi_directory], File['kakadu_library']],
  }

}
