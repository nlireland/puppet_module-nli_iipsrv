class iipsrv::params {
  $iipsrv_fcgi_directory = '/path/to/iipsrv/fcgi/binary'
  $fileserver_mount      = '/path/to/puppet/master/fileserver'
  $iipsrv_user           = 'puppetadmin'
  $iipsrv_group          = 'puppetadmin'
}
