# README #

A puppet module to install IIPImage Server binary and linked libraries from Puppet Master share.

## Usage ##

This module installs the IIPImage server by loading the necessary pre-compiled files (iipsrv.fcgi, libkdu_v64R.so) from the puppet master filserver. As such, it is most likely tightly-coupled to RHEL/CENTOS 7 and the specific directory paths used during compilation. This should be replaced by an rpm in the future.

The module should be used in combination with other puppet modules (e.g. lighttpd, memcached) to make up a fully functional IIPImage VM. See *Full server setup* example below.

The number of IIP processes, and the IIPImage parameters (e.g. JPEG_QUALITY, MAX_CVT, etc.) should be set in the server node definition when configuring the lighttpd instance.


### Minimal setup ###
```
#!puppet

node 'mynode' {
  # Install IIPImage files.
  class {'iipsrv':
    fileserver_mount      => 'files_mount',
    iipsrv_fcgi_directory => '/usr/local/iipsrv',
    iipsrv_user           => 'puppetadmin',
    iipsrv_group          => 'puppetadmin',
  }

}
```

### Full server setup ###
```
#!puppet
node 'mynode' inherits common {
  $iipsrv_watermark_path = '/usr/local/iipsrv/nli_logo_watermark_grey_gradient_40.tif'
  $www_root = '/var/www'
  $document_root = "$www_root/iipsrv"

  # Install Lighttpd
  class {'lighttpd':
    server_root      => "$www_root",
    document_root    => "$document_root",
  }

  # Setup a fastcgi instance for iipsrv
  lighttpd::fastcgi{'iipsrv':
     extension => '/fcgi-bin/iipsrv.fcgi',
     config => {
       "socket" => "/var/run/lighttpd/iipsrv-fastcgi.socket",
       "check-local" => "disable",
       "min-procs" => 1,
       "max-procs" => 8,
       "bin-path" => "/usr/local/iipsrv/iipsrv.fcgi",
       "bin-environment" => {
         "LOGFILE" => "/tmp/lighttpd-iipsrv.log",
         "VERBOSITY" => "5",
         "FILESYSTEM_PREFIX" => '/path/to/images/',
         "WATERMARK" => "$iipsrv_watermark_path",
         "WATERMARK_PROBABILITY" => '0.4',
         "WATERMARK_OPACITY"    => '0.15'
      },
    },
  }

  class {'memcached':
    listen_ip   => '127.0.0.1',
    user        => 'memcacheduser',
    max_memory  => 8192,
  }

  class {'iipsrv':
    fileserver_mount      => 'files_mount',
    iipsrv_fcgi_directory => '/usr/local/iipsrv',
    iipsrv_user           => 'iipuser',
    iipsrv_group          => 'iipuser',
  }

  file{'iipsrv_watermark':
    ensure  => present,
    owner  => $iipsrv_user,
    group  => $iipsrv_group,
    mode    => '0664',
    path    => "$iipsrv_watermark_path",
    source  => "puppet:///files_mount/nli_logo_watermark_grey_gradient_40.tif",
    require => Class['iipsrv'],
  }

  file{"$document_root":
    ensure  => directory,
    owner   => 'lighttpd',
    group   => 'lighttpd',
    require => Class['lighttpd'],
  }

}

```

## Old usage ##

A previous version of this module took a different approach to IIPImage setup, where the module started one or more iipsrv instance/services from the command which could then be configured as Nginx or Lighttpd upsteams. A reference branch of this implementation called 'old-instance-nginx-setup ' is available in the git repository, e.g. https://bitbucket.org/nlireland/puppet_module-nli_iipsrv/branch/old-instance-nginx-setup
